import cv2


img=cv2.imread('hojas.png',0)				#abro imagane
cv2.imshow('hojas.png',img)
cv2.waitKey(5)

umbral=200

for f in (range(img.shape[0])):				#recorro la imagen comparando cada pixel con un valor umbral
	for c in (range(img.shape[1])):			#f para filas, c para columnas
			if (img[f,c]<umbral):
				img[f,c]=0
			if (img[f,c]>=umbral):
				img[f,c]=255



cv2.imwrite('Img Segmentada.png',img)
cv2.imshow('Img Segmentada',img)
cv2.waitKey(0)
