#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pap.py
#  
#  Copyright 2022 ltdi <ltdi@ltdi-G00>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  



import sys
import cv2

#if (len(sys.argv)>1):
#	filename = sys.argv[1]
#else:
#	print ('Passa file name as first argument')
#	sys.exit(0)

cap=cv2.VideoCapture('video.mp4')

fourcc = cv2.VideoWriter_fourcc('X','V','I','D')

#framesize = (640,480)
alto=int(cap.get(3))
ancho=int(cap.get(4))
#frames=(alto,ancho)
out = cv2.VideoWriter('output.avi' , fourcc ,20.0 , (int(cap.get(3)),int(cap.get(4))))
delay=int (round((1/cap.get(cv2.CAP_PROP_FPS)*1000)))

while(cap.isOpened()):
	ret,frame=cap.read()
	if ret is True:
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		out.write(gray)
		cv2.imshow('Imagen gris', gray)
		if cv2.waitKey(delay) & 0xFF == ord ('q'):
			break
	else:
			break

cap.release()
out.release()
cv2.destroyAllWindows()
