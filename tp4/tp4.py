#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  tp4gaston.py
#  
#  Copyright 2022 Nazareno <nazareno@nazareno-HP-Laptop-14-bs0xx>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import cv2
import numpy as np

drawing = False # true if mouse is pressed
copia=False
mode = True # if True, draw rectangle. Press ’m’ to toggle to curve
modo1= False
ix=-1
iy=-1

def draw_circle(event , x, y, flags, param):
	global ix , iy , drawing , mode,modo1,copia
	if modo1 is False:
		if (event == cv2.EVENT_LBUTTONDOWN):
			drawing=True
			ix,iy=x,y
		elif event==cv2.EVENT_MOUSEMOVE:
			if drawing is True:
				if mode is True:
					cv2.rectangle(img, (ix,iy),(x,y),(0,255,0),-1)
				else:
					cv2.circle(img,(x,y),5,(0,0,255),-1)
		elif event==cv2.EVENT_LBUTTONUP:
			drawing=False
			if mode is True:
				cv2.rectangle(img,(ix,iy),(x,y),(0,255,0),-1)
			else:
				cv2.circle(img,(x,y),5,(0,0,255),-1)
	elif modo1 is True:
		if event==cv2.EVENT_LBUTTONDOWN:
			copia=True
			ix, iy=x,y
		elif event==cv2.EVENT_LBUTTONUP:
			if copia is True:
				if iy>y:
					if ix>x:
						imgcopia = img[y:iy, x:ix]
					else:
						imgcopia=img[y:iy, ix:x]
				else:
					if ix>x:
						imgcopia = img[iy:y, x:ix]
					else:
						imgcopia=img[iy:y, ix:x]
				
				cv2.imshow('img.copia', imgcopia)
				cv2.waitKey(0)
				copia=False


					
			
img=np.zeros((512,512,3),np.uint8) 


cv2.namedWindow('imagen')

cv2.setMouseCallback('imagen',draw_circle)
while(1):
	cv2.imshow('imagen',img)
	k=cv2.waitKey(1) & 0xFF
	if k==ord('m'):
		mode =not mode
	elif k==ord('g'):
		modo1 =not modo1
	elif k==ord('e'):
		 cv2.destroyWindow('img.copia')
	elif k==ord('q'):
		break
cv2.destroyAllWindows()

		
