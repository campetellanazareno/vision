import cv2
import numpy as np

#tp 6

def f2(img,ang,tx,ty,esc):
	
	alto, ancho=img.shape[0],img.shape[1]
	
	centro=((ancho/2),(alto/2))
	
	m=cv2.getRotationMatrix2D(centro,ang,esc)
	m[0][2]=+tx
	m[1][2]=+ty
	
	imgs = cv2.warpAffine(img, m, (ancho,alto))
	
	cv2.imshow('Transformacion',imgs)
	
	cv2.waitKey(0)
	cv2.destroyWindow('Transformacion')	
