import cv2
import numpy as np

#tp 5

def f1 (img,ang, tx, ty):
	
	alto, ancho=img.shape[0],img.shape[1]
	
	centro=((ancho/2),(alto/2))
	

	m=cv2.getRotationMatrix2D(centro,ang,1)
	m[0][2]=+tx
	m[1][2]=+ty
	img_r=cv2.warpAffine(img,m,(ancho,alto))

	cv2.imshow('imagen rot',img_r)
	cv2.waitKey(0)
	cv2.destroyWindow('Transformacion euclideana')


